@extends('layouts.auth')
@section('content')

<div class="container">

    <div class="page-header">
        <h1 class="page-title">
            {{-- --}}
        </h1>
    </div>

    {{-- <livewire:project-table/> --}}

    <div class="">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">pros</h3>
            </div>
            <div class="table-responsive">
                <table class="table card-table table-vcenter text-nowrap">
                    <thead>
                        <tr>
                            <th class="w-1">No.</th>
                            <th>Lnagkah</th>
                            <th>Catatan</th>
                            <th>Created</th>
                            <th>Status</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($project as $angka => $pro)

                        <tr>
                            <td><span class="text-muted">{{ 1 + $angka++ }}</span></td>
                            <td><a href="invoice.html" class="text-inherit">{{ $pro->nama_project }}</a></td>
                            <td>

                            </td>
                            <td>
                                15 Dec 2017
                            </td>
                            {{-- <td>
                                <span class="status-icon bg-success"></span> Paid
                            </td> --}}
                            <td class="text-right row">

                                <form action="{{route('project.destroy', $pro)}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="dropdown-item btn btn-sm btn-outline-primary">
                                        <i class="dropdown-icon fe fe-trash"></i>Delete
                                    </button>
                                    {{-- <li class="dropdown-item"><i class="dropdown-icon fe fe-trash"></i> Delete </li> --}}
                                </form>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
