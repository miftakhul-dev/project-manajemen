@extends('layouts.auth')
@section('content')

<div class="container">

    <div class="page-header">
        <h1 class="page-title">
            {{ $project->nama_project }}
        </h1>
    </div>

    <div class="row row-cards">
        <div class="col-sm-6 col-lg-3">
            <div class="card p-3 align-items-center">
                <div class="d-flex align-items-center">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        <i class="fe fe-message-square"></i> LINK PROJECT
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {{ $folder->deskripsi ?? '' }}
                                    @if ($folder == null)
                                    'tidak ada link project'
                                    @else
                                    <form action="{{route('folder.destroy', $folder)}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-primary">Hapus</button>
                                        {{-- <li class="dropdown-item"><i class="dropdown-icon fe fe-trash"></i> Delete </li> --}}
                                    </form>
                                    @endif

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{-- <livewire:project-table/> --}}

    <div class="">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Steps</h3>

                <div class="nav-item d-md-flex">
                    <button type="button" class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#datastep">
                        + Step
                    </button>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="datastep" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('step.save', $project->id) }}" method="post">
                                    @csrf
                                    <div class="mb-3">
                                        <label class="form-label">Step Name</label>
                                        <input type="text" class="form-control" name="nama_langkah" placeholder="Nama Project" required>
                                        @error('nama_langkah')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Catatan</label>
                                        <input type="text" class="form-control" name="catatan" placeholder="Deskripsi" required>
                                        @error('catatan')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    {{-- <button type="submit" class="btn btn-azure" ></button> --}}
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="nav-item d-md-flex">
                    <button type="button" class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#datalink">
                        + link Project
                    </button>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="datalink" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('folder.save', $project->id) }}" method="post">
                                    @csrf
                                    <div class="mb-3">
                                        <label class="form-label">Step Name</label>
                                        <input type="text" class="form-control" name="deskripsi" placeholder="Nama Project" required>
                                    </div>
                                    {{-- <button type="submit" class="btn btn-azure" ></button> --}}
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            <div class="table-responsive">
                <table class="table card-table table-vcenter text-nowrap">
                    <thead>
                        <tr>
                            <th class="w-1">No.</th>
                            <th>Lnagkah</th>
                            <th>Catatan</th>
                            <th>Created</th>
                            <th>Status</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($steps as $angka => $step)

                        <tr>
                            <td><span class="text-muted">{{ 1 + $angka++ }}</span></td>
                            <td><a href="invoice.html" class="text-inherit">{{ $step->nama_langkah }}</a></td>
                            <td>
                                {{ $step->catatan }}
                            </td>
                            <td>
                                15 Dec 2017
                            </td>
                            {{-- <td>
                                <span class="status-icon bg-success"></span> Paid
                            </td> --}}
                            <td class="text-right row">
                                <form action="{{route('step.update', $step->id)}}" class="mr-2" method="post">
                                    @csrf
                                    @method('put')
                                    <button class="btn btn-secondary btn-sm" type="submit"> {{ $retVal = ( $step->finished == 'Y' ) ? 'Belum Selesai' : 'Selesai' ;  }}</button>
                                </form>

                                <div class="dropdown">
                                    <button class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown">Actions</button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
