@extends('layouts.auth')
@section('content')

<div class="row">
    {{-- start profile client --}}
    <div class="col-4">
        <div class="card">
            <div class="card-body p-4 text-center">
                <span class="avatar avatar-xl mb-3 avatar-rounded"> J </span>
                <h3 class="m-0 mb-1"> {{$client->nama_client}} </a></h3>
                <div class="text-muted"> {{ $client->no_wa }} </div>
                <div class="mt-3">
                    <span class="badge bg-purple-lt">{{ $client->email }}</span>
                </div>
            </div>
            {{-- <div class="d-flex">
            <a href="#" class="card-btn">
                <!-- Download SVG icon from http://tabler-icons.io/i/mail -->
                <svg xmlns="http://www.w3.org/2000/svg" class="icon me-2 text-muted" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                    <rect x="3" y="5" width="18" height="14" rx="2" />
                    <polyline points="3 7 12 13 21 7" /></svg>
                Email</a>
            <a href="#" class="card-btn">
                <!-- Download SVG icon from http://tabler-icons.io/i/phone -->
                <svg xmlns="http://www.w3.org/2000/svg" class="icon me-2 text-muted" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                    <path d="M5 4h4l2 5l-2.5 1.5a11 11 0 0 0 5 5l1.5 -2.5l5 2v4a2 2 0 0 1 -2 2a16 16 0 0 1 -15 -15a2 2 0 0 1 2 -2" /></svg>
                Call</a>
        </div> --}}
        </div>
    </div>
    {{-- end profile client --}}


    <div class="col-8">
        {{-- start tabel project --}}
        <h3>Project</h3>
        <div class="card">
            <div class="table-responsive">
                <table class="table table-vcenter card-table">
                    <thead>
                        <tr>
                            <th>Nama Project</th>
                            <th>Harga</th>
                            <th>Nama Client</th>
                            <th>deadline</th>
                            <th class="w-1"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($projects as $project)
                        <tr>
                            <td>{{$project->nama_project}}</td>
                            <td class="text-muted">
                                {{$project->harga}}
                            </td>
                            <td class="text-muted"><a href="#" class="text-reset"> {{$project->client->nama_client}} </a></td>
                            <td class="text-muted">
                                {{$project->deadline}}
                            </td>
                            <td>
                                <div class="btn-list flex-nowrap">
                                    <div class="dropdown">
                                        <button class="btn dropdown-toggle align-text-top" data-bs-toggle="dropdown">
                                            Actions
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-end">
                                            <form action="{{route('project.destroy', $project)}}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button class="dropdown-item" type="submit">Hapus</button>
                                            </form>
                                            <form action="{{route('project.invoice', $project)}}" method="post">
                                                @csrf
                                                <button class="dropdown-item" type="submit"> Kirim </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{-- end tabel project --}}

        {{-- start edit client --}}
        <div class="card mt-3">
            <form action="{{route('client.update', $client)}}" method="post">
                @csrf
                @method('put')
                <div class="modal-body">
                    <div class="mb-3">
                        <label class="form-label">Nama Client</label>
                        <input type="text" value="{{$client->nama_client}}" class="form-control" name="nama_client" placeholder="Nama Client">
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="form-label">Nomor WA</label>
                                <input type="text" value="{{$client->no_wa}}" name="no_wa" placeholder="Nomor WA" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="form-label">Email</label>
                                <input type="email" value="{{$client->email}}" name="email" placeholder="E-mail" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary ms-auto">
                        Update Client
                    </button>
                </div>
            </form>
        </div>
        {{-- start edit client --}}

    </div>

</div>
@endsection
