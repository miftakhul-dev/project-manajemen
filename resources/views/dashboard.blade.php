@extends('layouts.auth')
@section('content')


<div class="my-3 my-md-5">
    <div class="container">

        {{-- start tabel finished payment --}}
        <div class="col-12">
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-vcenter card-table">
                        <thead>
                            <tr>
                                <th>Nama Project</th>
                                <th>Info</th>
                                <th>Harga</th>
                                <th>Tanggal Terbayar</th>
                                <th class="w-1"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($payments as $payment)

                            <tr>
                                <td>{{$payment->project_id}}</td>
                                <td class="text-muted">
                                    {{$payment->terbayar}}
                                </td>
                                <td class="text-muted"><a href="#" class="text-reset">harga</a></td>
                                <td class="text-muted">
                                    {{$payment->created_at}}
                                </td>
                                <td>
                                    <a href="#">
                                        {{$payment->transaction_id}}
                                    </a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {{-- end tabel finished payment --}}



        {{-- start tabel your project --}}
        <div class="col-12 mt-3">
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-vcenter card-table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Harga</th>
                                <th class="w-1"></th>
                                <th class="w-1"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($projects as $project)
                            <tr>
                                <td>{{$project->nama_project}}</td>
                                <td class="text-muted">
                                    {{$project->harga}}
                                </td>
                                <td>
                                    <div class="btn-list flex-nowrap">
                                        <div class="dropdown">
                                            <button class="btn dropdown-toggle align-text-top" data-bs-toggle="dropdown">
                                                Actions
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                <a class="dropdown-item" href="{{route('project.step', $project)}}">
                                                    Show/Edit Step
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="btn" data-bs-toggle="modal" data-bs-target="#step{{$project->id}}">
                                        Input Perkembangan
                                    </a>

                                    <div class="modal modal-blur fade" id="step{{$project->id}}" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Langkah</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <form action="{{route('step.simpan', $project)}}" method="post">
                                                    @csrf
                                                    @method('put')
                                                    <div class="modal-body row">
                                                        <div class="mb-3 col-6">
                                                            <label class="form-label">Nama Langkah</label>
                                                            <input type="text" class="form-control" name="nama_langkah" placeholder="Nama Project">
                                                        </div>
                                                        <div class="mb-3 col-6">
                                                            <label class="form-label">Selesai</label>
                                                            <input type="checkbox" name="finished" class="form-check-input">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                                                            close modal
                                                        </a>
                                                        <button type="submit" class="btn btn-primary ms-auto">
                                                            + Buat
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {{-- end tabel your project --}}


    </div>
</div>



@endsection
