/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Alpine from 'alpinejs'

window.Alpine = Alpine

// import 'jquery'
import './../../vendor/power-components/livewire-powergrid/dist/powergrid'
// import 'select2'


Alpine.start()

window.Vue = require('vue').default;

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

const app = new Vue({
    el: '#app',
});
