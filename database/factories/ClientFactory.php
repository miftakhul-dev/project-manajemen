<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => random_int(1, 10),
            'nama_client' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'no_wa' => $this->faker->phoneNumber( range(1, 9) ),
        ];
    }
}
