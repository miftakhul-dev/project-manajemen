<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = ['nama_client', 'email', 'no_wa',];

    public function Projects()
    {
        return $this->hasMany(Project::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
