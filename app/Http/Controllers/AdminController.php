<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Payment;
use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function index()
    {
        $users = User::get(['id', 'name', 'email']);
        $projects = Project::get(['id', 'nama_project', 'harga', 'client_id', 'deadline']);
        $clients = Client::get();

        // $val = (isset($project->folder)) ? '<i class=" fa-solid fa-check"></i>' : 'tidak';
        // $retVal = (isset($projects->find(17)->folder)) ? a : b ;
        // dd(isset($projects->find(17)->folder));


        return view('admin.index', [
            'projects' => $projects,
            'users' => $users,
            'clients' => $clients,
            'payments' => Payment::get(),

        ]);
    }

    public function destroy(User $user)
    {
        $user->delete();
        return back();
    }
}
